FROM openjdk:8-jre-alpine

WORKDIR /docker

COPY target/Api-Investimentos*.jar Api-Investimentos.jar

CMD ["java", "-jar", "Api-Investimentos.jar"]